﻿namespace Owin
{
    using Jhong.OWINSession;

    public static class AppBuilderExtension
    {
        public static IAppBuilder UseOWINSession(this IAppBuilder app, OWINSessionOption option)
        {
            OWINSessionOption.OSOption = option;
            app.Use(typeof(OWINSessionMiddleware));
            return app;
        }

        public static IAppBuilder UseOWINSession(this IAppBuilder app)
        {
            return app.UseOWINSession(new OWINSessionOption());
        }
    }
}
﻿namespace Jhong.OWINSession.InProcess
{
    using System;

    internal class InProcessFactory
    {
        private InProcessFactory()
        {
        }

        private static SessionCache _cacheInstance;

        static InProcessFactory()
        {
            _cacheInstance = new SessionCache();
        }

        public static Func<ISessionOpera> InProcessInstance = () => _cacheInstance;
    }
}
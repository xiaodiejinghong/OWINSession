﻿namespace Jhong.OWINSession.OWINCookie
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public class RequestCookieCollection : IEnumerable<KeyValuePair<string, string>>, IEnumerable
    {
        private IDictionary<string, string> Store
        {
            get;
            set;
        }

        public string this[string key]
        {
            get
            {
                string result;
                this.Store.TryGetValue(key, out result);
                return result;
            }
        }

        public RequestCookieCollection(IDictionary<string, string> store)
        {
            if (store == null)
            {
                throw new ArgumentNullException("store");
            }
            this.Store = store;
        }

        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return this.Store.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
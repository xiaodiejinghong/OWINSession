﻿namespace Jhong.OWINSession.OWINCookie
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using Env = System.Collections.Generic.IDictionary<string, object>;

    public class ResponseCookieCollection
    {
        private IDictionary<string, string[]> _header;

        public ResponseCookieCollection(Env env)
        {
            this._header = env["owin.ResponseHeaders"] as IDictionary<string, string[]>;
        }

        public void Append(string key, string value)
        {
            AppendHeaderUnmodified(this._header, "Set-Cookie", new string[]
			{
				Uri.EscapeDataString(key) + "=" + Uri.EscapeDataString(value) + "; path=/"
			});
        }

        public void Append(string key, string value, CookieOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException("options");
            }
            bool flag = !string.IsNullOrEmpty(options.Domain);
            bool flag2 = !string.IsNullOrEmpty(options.Path);
            bool hasValue = options.Expires.HasValue;
            string text = string.Concat(new string[]
			{
				Uri.EscapeDataString(key),
				"=",
				Uri.EscapeDataString(value ?? string.Empty),
				(!flag) ? null : "; domain=",
				(!flag) ? null : options.Domain,
				(!flag2) ? null : "; path=",
				(!flag2) ? null : options.Path,
				(!hasValue) ? null : "; expires=",
				(!hasValue) ? null : (options.Expires.Value.ToString("ddd, dd-MMM-yyyy HH:mm:ss ", CultureInfo.InvariantCulture) + "GMT"),
				(!options.Secure) ? null : "; secure",
				(!options.HttpOnly) ? null : "; HttpOnly"
			});
            AppendHeaderUnmodified(this._header, "Set-Cookie", new string[]
			{
				text
			});
        }

        private static void AppendHeaderUnmodified(IDictionary<string, string[]> headers, string key, params string[] values)
        {
            if (values == null || values.Length == 0)
            {
                return;
            }
            string[] headerUnmodified = GetHeaderUnmodified(headers, key);
            if (headerUnmodified == null)
            {
                SetHeaderUnmodified(headers, key, values);
                return;
            }
            SetHeaderUnmodified(headers, key, headerUnmodified.Concat(values));
        }

        private static void SetHeaderUnmodified(IDictionary<string, string[]> headers, string key, IEnumerable<string> values)
        {
            if (headers == null)
            {
                throw new ArgumentNullException("headers");
            }
            headers[key] = values.ToArray<string>();
        }

        private static string[] GetHeaderUnmodified(IDictionary<string, string[]> headers, string key)
        {
            if (headers == null)
            {
                throw new ArgumentNullException("headers");
            }
            string[] result;
            if (!headers.TryGetValue(key, out result))
            {
                return null;
            }
            return result;
        }
    }
}
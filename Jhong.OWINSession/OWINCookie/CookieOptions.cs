﻿namespace Jhong.OWINSession.OWINCookie
{
    using System;

    public class CookieOptions
    {
        public string Domain
        {
            get;
            set;
        }

        public string Path
        {
            get;
            set;
        }

        public DateTime? Expires
        {
            get;
            set;
        }

        public bool Secure
        {
            get;
            set;
        }

        public bool HttpOnly
        {
            get;
            set;
        }

        public CookieOptions()
        {
            this.Path = "/";
        }
    }
}
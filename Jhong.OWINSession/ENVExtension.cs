﻿namespace Jhong.OWINSession
{
    using Env = System.Collections.Generic.IDictionary<string, object>;

    internal static class ENVExtension
    {
        public static T Get<T>(this Env env, string key)
        {
            object obj;
            if (!env.TryGetValue(key, out obj))
            {
                return default(T);
            }
            return (T)((object)obj);
        }

        public static void Set<T>(this Env env, string key, T value)
        {
            env[key] = value;
        }
    }
}
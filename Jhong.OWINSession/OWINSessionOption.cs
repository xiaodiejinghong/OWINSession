﻿namespace Jhong.OWINSession
{
    public class OWINSessionOption
    {
        /// <summary>
        /// 保存Session的方式
        /// </summary>
        public SessionMode SessionMode { get; set; }

        private int? _sessionExpiredMinute;

        /// <summary>
        /// 过期时间
        /// </summary>
        public int? SessionExpiredMinute
        {
            get
            {
                return null == this._sessionExpiredMinute ? 30 : this._sessionExpiredMinute;
            }
            set
            {
                this._sessionExpiredMinute = value;
            }
        }

        private int? _maxSessionNum;

        /// <summary>
        /// 最大Session保存数
        /// </summary>
        public int? MaxSessionNum
        {
            get
            {
                return null == this._maxSessionNum ? 10000 : this._maxSessionNum;
            }
            set
            {
                this._maxSessionNum = value;
            }
        }

        public static readonly string SessionIDName = "Jhong_OWINSession";

        public static OWINSessionOption OSOption { get; set; }
    }
}
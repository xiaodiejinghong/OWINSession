﻿namespace Jhong.OWINSession
{
    internal interface ISessionManager
    {
        object GetSession(string sessionID, string key);

        void SetSession(string sessionID, string key, object value);
    }
}
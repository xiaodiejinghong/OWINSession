﻿namespace Jhong.OWINSession
{
    internal interface ISessionOpera
    {
        Session GetSession(string sessionID);

        void SetSession(string sessionID, Session session);

        void SetTimeOut(string sessionID);

        bool CheckExists(string sessionID);
    }
}
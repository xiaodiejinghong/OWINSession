﻿using System;
using System.Collections.Generic;

namespace Jhong.OWINSession
{
    public class Session
    {
        public object this[string key]
        {
            get
            {
                object obj = null;
                this._userData.TryGetValue(key, out obj);
                return obj;
            }
            set
            {
                this._userData[key] = value;
            }
        }

        private IDictionary<string, object> _userData = new Dictionary<string, object>();

        internal DateTime DeathTime { get; set; }
    }
}
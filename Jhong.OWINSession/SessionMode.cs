﻿namespace Jhong.OWINSession
{
    /// <summary>
    /// 保持Session的方式
    /// </summary>
    public enum SessionMode
    {
        /// <summary>
        /// 进程内
        /// </summary>
        InProcess,

        /// <summary>
        /// 状态服务器
        /// </summary>
        StateServer
    }
}
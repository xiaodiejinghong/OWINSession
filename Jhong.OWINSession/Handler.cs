﻿namespace Jhong.OWINSession
{
    using Env = System.Collections.Generic.IDictionary<string, object>;

    public static class Handler
    {
        public static void Hand(Env env)
        {
            var manager = DefaultSessionManager.Manager;
            manager.UpdateTimeOut(env);
        }
    }
}
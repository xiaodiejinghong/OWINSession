﻿namespace Jhong.OWINSession
{
    using System.Threading.Tasks;
    using APPFunc = System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>;
    using Env = System.Collections.Generic.IDictionary<string, object>;

    public class OWINSessionMiddleware
    {
        private readonly APPFunc _next;

        public OWINSessionMiddleware(APPFunc next)
        {
            this._next = next;
        }

        public Task Invoke(Env env)
        {
            Handler.Hand(env);
            return this._next(env);
        }
    }
}
﻿namespace Jhong.FunMVC
{
    using OWINSession;
    using System;

    public static class ControllerExtension
    {
        public static object GetSession(this Controller controller, string key)
        {
            return DefaultSessionManager.Manager.GetSession(controller.GetSessionID(), key);
        }

        public static void SetSession(this Controller controller, string key, object value)
        {
            DefaultSessionManager.Manager.SetSession(controller.GetSessionID(), key, value);
        }

        private static string GetSessionID(this Controller controller)
        {
            string sessionID;
            sessionID = controller.Request.Cookie[OWINSession.OWINSessionOption.SessionIDName];
            if (string.IsNullOrWhiteSpace(sessionID)) sessionID = controller.Request.Headers.Get("tmpSessionID");
            if (string.IsNullOrWhiteSpace(sessionID)) throw new ArgumentNullException("Can not get Session ID");
            return sessionID;
        }
    }
}